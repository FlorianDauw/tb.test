﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace exo1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Entrez le nombre de secondes à convertir");
            int secondes = int.Parse(Console.ReadLine());

            int heures = secondes / 3600;
            secondes %= 3600;

            int minutes = secondes / 60;
            secondes %= 60;

            Console.WriteLine($"{heures}H {minutes}min {secondes}sec");
            Console.ReadKey();
        }
    }
}
